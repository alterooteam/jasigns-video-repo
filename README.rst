Overview
===========
This video repo is for serving videos
Videos are served from the 'static/' directory

They can be accessed using the form:

    https://jasigns-1045.appspot.com/{videoname}.mp4

(http also works as a protocol).

Assumptions
=============
The `rename` command is installed and
and `appcfg.py` (provided by the appengine sdk) is available in your path.

Usage
=========
The `push.sh` script replaces spaces and brackets "()" in file names with underscores
and then pushes to the `jasigns-1405` appengine app.

1. Add you mp4 videos to the static folder.

2. While in the root of the repo run the following::

    ./video.sh -u

That's it


App Development
===================

1. Install dependencies in the project's lib directory.
   Note: App Engine can only import libraries from inside your project directory.

   ```
   cd path/to/project
   pip install -r requirements.txt -t lib
   ```
2. Run this project locally from the command line:

   ```
   dev_appserver.py .
   ```
