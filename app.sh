#!/bin/bash

filename="videolist.txt"
videofolder=static/

rename_videos(){
  echo "Rename all videos in the static folder."
  mkdir -p ../discard
  find ./static -depth -name "*)*" -execdir rename "s/\)/_/g" "{}" \;
  find ./static -depth -name "*(*" -execdir rename "s/\(/_/g" "{}" \;
  find ./static -depth -name "* *" -execdir rename "s/ /_/g" "{}" \;
  find ./static -depth -name "*_NDS*" -execdir rename "s/_NDS//g" "{}" \;
  find ./static -depth -name "* *" -execdir mv "{}" ../discard  \;
}

download_videos(){
  echo "----------------------------------------------------"
  echo "Download the list of videos that are in the videolist.txt"
  while read -r line; do
    wget "$app_url/p/$line" -P $(videofolder)
  done < "$filename"
  echo "---------------------------------------------------"
}

update_video_list(){
  rename_videos
  echo "----------------------------------------------------"
  echo "Update the list of videos that are in the static folder"
  catalogue=`cat $filename`
  for video in $(ls static/*.mp4 | xargs -n1 basename); do
    if [[ $catalogue != *"$video"* ]]; then
      echo "#####################################"
      echo $video >> $filename;
      echo "$video added to $filename"
      echo "#####################################"
    fi
  done
  echo "---------------------------------------------------"
}

update_app(){
  update_video_list
  appcfg.py update .
}

install_app(){
  # Install virtualenv
  if [ -z $(which pip) ]; then \
  	sudo apt-get install python-pip; \
  fi

	if [ -z $(which node) ]; then
	    curl -sL https://deb.nodesource.com/setup | sudo bash -
	    sudo apt-get install -y nodejs
	fi

	if [ -z $(which npm) ]; then
	    sudo apt-get install -y npm
	fi

	if [ -z $(which bower) ]; then
	    sudo npm install -g bower
	fi
	
  # Install dependencies
  pip install -r requirements.txt -t lib
  bower install

}

run_app(){
  dev_appserver.py .
}

run_c9_app(){
  dev_appserver.py . --host "0.0.0.0"
}



help_commands(){
  echo "-d     Download videos from appengine"
  echo "-u     Update app"
  echo "-e     Update video list"
  echo "-i     Install application"
  echo "-x     Run application"
  echo "-h     For help"
}


#########################################
## Accept command line agruments
#########################################

while getopts ":d :u :e :i :x :z" opt; do
  case $opt in
    u)
      update_app
      ;;
    e)
      update_video_list
      ;;
    d)
      download_videos
      ;;
    i)
      install_app
      ;;
    x)
      run_app
      ;;
    z)
      run_c9_app
      ;;
    \?)
      echo "Invalid option: -$OPTARG"  | tee -a $BUILD_LOG_FILE
      echo "Available agruments are:"  | tee -a $BUILD_LOG_FILE
      help_commands
      ;;
  esac
done
