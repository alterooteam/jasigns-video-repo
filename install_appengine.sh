#!/bin/bash

APPENGINE_PATH=/usr/local/google_appengine
APPENGINE_APPCFG=/usr/local/google_appengine/appcfg.py
APP_PATH=$(pwd)
USER=$(whoami)
USER_HOME=/home/${USER}
TARGET_ZIP=google_appengine_1.9.40.zip
APPENGINE_DLOAD=${USER_HOME}/${TARGET_ZIP}

if [ -z $(which curl) ]; then \
	sudo apt-get install curl; \
fi

if [ -z $(which pip) ]; then \
	sudo apt-get install python-pip; \
	sudo pip install flake8; \
fi

if [ -z $(which virtualenv) ]; then \
	sudo apt-get install python-virtualenv; \
fi

if [ ! -e "${APPENGINE_DLOAD}" ]; then \
	wget https://storage.googleapis.com/appengine-sdks/featured/${TARGET_ZIP} -P ${USER_HOME}; \
	unzip ${APPENGINE_DLOAD} -d ${USER_HOME}; \
fi

if [ ! -e "${APPENGINE_PATH}" ]; then \
	sudo ln -s ${USER_HOME}/google_appengine ${APPENGINE_PATH}; \
fi

if [ -z $(which appcfg.py) ]; then \
    echo "Exporting appengine to path."
    sudo echo "PATH=$PATH:${APPENGINE_PATH}" >> /etc/profile; \
    sudo echo "export PATH" >> /etc/profile; \
fi



