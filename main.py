"""`main` is the top level module for your Flask application."""

import os

# Import the Flask Framework
from flask import Flask, render_template
from firebase import firebase

app = Flask(__name__)
# Note: We don't need to call run() since our application is embedded within
# the App Engine WSGI application server.

# Jinja2 templates commenting tags
app.jinja_env.line_statement_prefix = '#'
app.jinja_env.line_comment_prefix = '##'

firebase = firebase.FirebaseApplication('https://jasigns-1045.firebaseio.com/', None)


@app.route('/')
def hello():
    """Return a friendly HTTP greeting."""
    return  render_template(
        "index.html"
    )


@app.route('/catalogue')
def firebase_catalogue():
    catalogue = firebase.get('/videoCatalogue', None,
         params={'print': 'pretty'},
         headers={'X_FANCY_HEADER': 'very fancy'})
    category = firebase.get('/categoryInfo', None,
         params={'print': 'pretty'},
         headers={'X_FANCY_HEADER': 'very fancy'})
    return render_template(
        "catalogue.html",
        catalogue=catalogue,
        category=category
    )



@app.route('/video/list/')
def video_list():
    """Return a list of videos"""
    cwd = os.path.dirname(__file__)
    static = os.path.join(cwd, "static")
    videos = [f for f in os.listdir(static) if f.endswith("mp4")]
    return render_template(
        "video_list.html",
        videos=videos
    )


@app.errorhandler(404)
def page_not_found(e):
    """Return a custom 404 error."""
    return 'Sorry, Nothing at this URL.', 404


@app.errorhandler(500)
def application_error(e):
    """Return a custom 500 error."""
    return 'Sorry, unexpected error: {}'.format(e), 500
